(autoload 'se/make-summary-buffer "summarye" nil t)

(define-key-after (lookup-key global-map
			      (if (< 20 emacs-major-version)
				  [menu-bar tools]
				  [menu-bar search]))
  [se:make-summary]
  '("Make summary" . se/make-summary-buffer)
  t)

(define-key-after (lookup-key global-map
			      (if (< 20 emacs-major-version)
				  [menu-bar tools]
				  [menu-bar search]))
  [se:check-document]
  '("Load se-doc" . (lambda () (interactive) (load-library "se-doc")))
  t)
