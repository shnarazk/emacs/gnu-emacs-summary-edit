;;; se-jpn.el -*-emacs-lisp-*-  -*-coding: iso-2022-7bit; -*-
;;; Japanese document browser using se-doc
;;; Author: Shuji Narazaki (narazaki@InetQ.or.jp)
;;; Version: 1.7.5
;;; Time-stamp: <2005-06-29 14:43:33 narazaki>
;;; Keywords: text

;;; Commentary:
;;; Emacs lisp$BHGJ8>O?dZJ;Y1g%W%m%0%i%`(B ($BF|K\8lMQ(B)

;;; CHANGE LOG
;;; narazaki@csce.kyushu-u.ac.jp		Sat Sep  3 23:23:37 1994
;;;  Version 1.0
;;; narazaki@csce.kyushu-u.ac.jp		Fri Oct 22 19:15:19 1993
;;;  Version 0.1

;;; Code:
(eval-and-compile
  (require 'summarye)
  (require 'se-doc))
(defvar marker)
;;; $BF|K\8l$K4X$9$k$b$N$O1Q>.J8;z$K8BDj$9$k!%(B
;; $B1QBgJ8;z$O1Q8l$N$?$a$KM=Ls!%(B

(defconst se-doc/j/alphabet "E$B1QC18l(B")
(defconst se-doc/j/multi-deny "d$BJ#?tH]Dj(B")
(defconst se-doc/j/setsuzoku-ga "g$B@\B3;l!V$,!W(B")
(defconst se-doc/j/multi-ha "h$B!V$O!WJ#?t(B")
(defconst se-doc/j/1st-term "j$B=i=P=Q8l(B")
(defconst se-doc/j/katakana "k$B%+%?%+%J(B")
(defconst se-doc/j/multi-no "n$B!V$N!WJ#?t(B")
(defconst se-doc/j/suushi "s$B?t;l(B")
(defconst se-doc/j/toritate "t$B$H$j$?$F;l(B")
(defconst se-doc/j/ukemi "u$B<u?H(B")
(defconst se-doc/j/youni+deny "y$B!V$h$&$K!W!\H]Dj(B")
(defconst se-doc/j/machigai "m$BBg?M$N>o<1(B")

(setq se-doc/show-at-second-line 
      (append (list se-doc/shead se-doc/stail
		    se-doc/j/ukemi se-doc/j/katakana
		    se-doc/j/1st-term se-doc/j/alphabet
		    se-doc/j/toritate se-doc/j/setsuzoku-ga se-doc/j/multi-ha
		    se-doc/j/youni+deny se-doc/j/multi-deny se-doc/j/multi-no)
	      se-doc/category-list))

;;; $B<u?H(B
(se-doc/define-category se-doc/j/ukemi
  ;; $B0J2<$N8!=P>r7o$K$OLZB<@t!$%o!<%W%m:nJ85;=Q!$4dGH?7=q!$4dGH=qE9!$(B1993$B!%(B
  ;; $B$N(Bpp.214-215$B$K0zMQ$5$l$F$$$k%"%k%4%j%:%`$r;H$C$F$$$k!%(B
  "\\([$B$+$5$?$^$i$o$,$P(B]\\|[$B$7;`(B]\n?$B$J(B\\)\n?$B$l(B")

(se-doc/define-method se-doc/j/ukemi (beg end cat)
  (let* ((s-beg (se-doc/this-sentence-beg beg se-doc/frame-width-max))
	 (sentence (buffer-substring-no-properties s-beg end)))
    (setq sentence (se-doc/strip-latex (se-doc/canonical-sentence sentence)))
    (set-marker marker beg)
    (goto-char (se-doc/skip-whitespace end))
    (when (se-doc/valid-ukemi-p sentence)
      (se/set-face beg end nil t)
      (se-doc/tail-display-string sentence))))

(defun se-doc/valid-ukemi-p (str)
  (not 
   (string-match "\\($B$D$+(B\\|$B4E$C$?(B\\|$B$o$l$o(B\\|$B@8$^(B\\|$BJ,$+(B\\|$B8=$o(B\\|$BI=$o(B\\)$B$l(B" str)))

(se-doc/define-documentation se-doc/j/ukemi "
$B<u?H$,B?$$J8>O$O!D(B")

;;; $B%+%?%+%J(B
(se-doc/define-category se-doc/j/katakana "\\cK\n?\\(\\(\\cK\\|$B!&(B\\)\n?\\)+\\cK")

(se-doc/define-method se-doc/j/katakana (beg end cat)
  (se/set-face beg end nil nil)
  (se/remove-regexp-in-string "\n" (buffer-substring-no-properties beg end)))

;;; $B=i=P=Q8l(B
(se-doc/define-category se-doc/j/1st-term
;;  "\\(\\cK+\\|\\cC+\\)\n?\\(\\cK+\n?\\|\\cC+\n?\\|[A-Za-z0-9]+\n?\\)+\\(\\cK\\|\\cC\\)+"
;; $BDL?.Gr=q(B367321byte$B$G%F%9%H$9$k$H!$(B
;; $B%j%9%H(B	19:50
;; $B%O%C%7%eI=(B	18:36
;; $B$o$:$+$J$,$i%O%C%7%eI=$NJ}$,B.$$!%(B
  "\\(\\cK\\|\\cC\\)\n?\\(\\(\\cK\\|\\cC\\|[A-Za-z0-9]\\)\n?\\)+\\(\\cK\\|\\cC\\)"
;; $B$=$7$F8=:_$N@55,I=8=$KJQ99$9$k$H(B:
;;               2:25	(5891$B8D8!=P(B)
  )
(se-doc/define-method se-doc/j/1st-term (beg end cat)
  (let* ((pattern (se/remove-regexp-in-string "\n" (buffer-substring-no-properties beg end)))
	 (result (se/only-once pattern)))
    (if result (se/set-face beg end nil t))
    result))

(se-doc/define-documentation se-doc/j/1st-term "
$B2J3X5;=QJs9p=q$GL$Dj5A=Q8l$r;H$&$3$H$J$+$l!%(B")

;;; $B1QJ8;z(B
(se-doc/define-category se-doc/j/alphabet "[A-Za-z][A-Za-z0-9][A-Za-z0-9]+")

(se-doc/define-method se-doc/j/alphabet (beg end cat)
  (se/set-face beg end nil nil)
  (buffer-substring-no-properties beg end))

;;; $B?t;l(B
(se-doc/define-category se-doc/j/suushi
  "[0-9$B#0(B-$B#90l(B-$B6e==I4@iK|2/C{(B]+\n?[,$B!"(B0-9$B#0(B-$B#90l(B-$B6e==I4@iK|2/C{(B]*[$B$D8D?MBN2sKgG\F|7n1_(B]")
(se-doc/define-method se-doc/j/suushi (beg end cat)
  (se/set-face beg end nil nil)
  (se-doc/simple-display-string (buffer-substring-no-properties beg end)))

;;; $B$H$j$?$F;l(B
(se-doc/define-category se-doc/j/toritate
  "\\($B$G(B\n?$B$b(B\\|$B$J(B\n?$B$I(B\\|$B$^(B\n?$B$G(B\\|$B$@(B\n?$B$1(B\\)")

(se-doc/define-method se-doc/j/toritate (beg end cat)
  (let* ((s-beg (se-doc/this-sentence-beg beg se-doc/frame-width-max))
	 (s-end (se-doc/this-sentence-end end se-doc/frame-width-max)))
    (se/set-face beg end nil t)
    (se-doc/simple-display-string (buffer-substring-no-properties s-beg s-end))))

(se-doc/define-documentation se-doc/j/toritate "
$B$J$K$,$$$1$J$$$N$@$C$1(B?")

;;; $B@\B3;l!V$,!W(B
(se-doc/define-category se-doc/j/setsuzoku-ga
  ;; \\1$B$,BP>]$N!V$,!W(B
  "[$B$&$/$9$D$L$`$k$0$V$$$@$?$s(B]\n?\\($B$,(B\\)\n?[^$B$C%C$s%s(B]")

(se-doc/define-method se-doc/j/setsuzoku-ga (beg end cat)
  (let* ((ga-beg (match-beginning 1))
	 (ga-end (match-end 1))
	 (s-beg (se-doc/this-sentence-beg beg se-doc/frame-width-max))
	 (s-end (se-doc/this-sentence-end end se-doc/frame-width-max)))
    (when (se-doc/valid-setsuzoku-ga-p beg end)
      (se/set-face ga-beg ga-end nil t)
      (se-doc/simple-display-string (buffer-substring-no-properties s-beg s-end)))))

(defun se-doc/valid-setsuzoku-ga-p (beg end)
  (save-excursion
    (save-restriction
      (let ((flag t))
	(goto-char beg)
	(backward-char 4)		; $B!V$[!W!\!V$&!W!\Fs$D$N2~9T(B
	(narrow-to-region (point) end)	; set boundary for looking-at
	;; $B>r7o(B4
	(if (looking-at ".*$B$[(B\n?$B$&(B\n?$B$,(B") (setq flag nil))
	;;$B>r7o(B5  0$B$D$H$O8@$o$J$$$N$G!$(B0$B$N%P%j%(!<%7%g%s$OH4$$$?!%(B
	(if (looking-at ".*[1-9$B#1#2#3#4#5#6#7#8#90lFs;0;M8^O;<7H,6e(B]\n?$B$D(B\n?$B$,(B")
	    (setq flag nil))
	flag))))

(se-doc/define-documentation se-doc/j/setsuzoku-ga "
$B@\B3=u;l$N!V$,!W$K$O!$!V$7$+$7!W!V$=$l$f$(!W!V$=$7$F!W$N0UL#$r;}$DMQK!$,$"(B
$B$k$N$G!$A08e4X78$N$o$+$j$K$/$$$3$H$,$"$j$^$9!%!X?dZJ!Y$r;H$&$3$H$G!$@\B3=u(B
$B;l!V$,!W$N8=$l$?2U=j$GJ8$rJ,3d$9$k$+$I$&$+$N8!F$$,MF0W$K$J$j$^$9!%(B
		    			$B!X?dZJ!Y(B($B4dGH=qE9(B)$B$N9-9p$h$j(B

 $B%"%k%4%j%:%`(B:
  1. $B!V$,!W$,@\B3=u;l$G$"$k$?$a$K$O!V$,!W$N(B1$BJ8;zA0$,!V$&!$$/!$$9!$$D!$$L!$(B
     $B$`!$$k!$$0!$$V!$$$!$$@!$$?!$$s!W$N$$$:$l$+$G$J$1$l$P$J$i$J$$!%(B
  2. $B!V$,!W$N(B1$BJ8;z8e$,B%2;!$$O$D2;$G$"$k$P$"$$!$$=$N!V$,!W$O@\B3=u;l$G$J$$!%(B
  3. $BJ8F,$,!V$@$,!W$G$"$k$H$-!$$=$N!V$,!W$O@\B3=u;l$G$J$$!%(B
  4. $B!V$,!W$N(B1$BJ8;zA0$,!V$&!W$G$"$k$H$-!$$=$N!V$&!W$N(B1$BJ8;zA0$,!V$[!W$G$"(B
     $B$l$P!$$=$N!V$,!W$O@\B3=u;l$G$J$$!%(B
  5. $B!V$,!W$N(B1$BJ8;zA0$,!V$D!W$G$"$k$H$-!$$=$N!V$D!W$N(B1$BJ8;zA0$,?t;z$^$?$O(B
     $B4A?t;z$G$"$l$P!$$=$N!V$,!W$O@\B3=u;l$G$J$$!%(B
  $B2<1`!$?{>B!$5mEg(B : $BF|K\8lJ8>O;Y1g%D!<%k!X?dZJ!Y$K$*$1$k=u;l!V$,!W$NCj(B
  $B=P$K$D$$$F!$>pJs=hM}3X2qO@J8;o!$(BVol.35$B!$(BNo.8$B!$(Bpp.1652-1660$B!$(B1994$B!%(B
  $B>e5-$N%"%k%4%j%:%`$G<h$j07$($J$$$b$N(B:
  $B!&(B $B<h07$$$,(B
  $B!&(B $B0c$$$,(B
  $B!&(B $B$^$?$,$k(B")

;;; $B!V$O!W(B
(se-doc/define-category se-doc/j/multi-ha
  "$B$O(B\n?\\([^$B!%(B.$B!#(B?$B!)(B\n$B$O(B]\\|\n[^$B!%(B.$B!#(B?$B!)(B\n]\\|[.?][^ 	\n]\\)+$B$O(B")

(se-doc/define-method se-doc/j/multi-ha (beg end cat)
  (let* ((s-beg (se-doc/this-sentence-beg beg 7))
	 (phrase (buffer-substring-no-properties s-beg end)))
    (and (se-doc/valid-multi-ha phrase)
	 (se-doc/simple-display-string phrase))))

(defun se-doc/valid-multi-ha (str)
  (setq str (se/remove-regexp-in-string "[$B$G$K(B]$B$O(B" str))
  (setq str (se/remove-regexp-in-string "[^$B$O(B]+" str))
  (< 4 (string-width str)))

;;; $B!V$h$&$K!W!\H]Dj(B
(se-doc/define-category se-doc/j/youni+deny
  "$B$h(B\n?$B$&(B\n?$B$K(B\n?\\([^$B!%(B.$B!#(B?$B!)(B\n$B$J(B]\\|$B$J(B\n?[^$B$$$/$7(B]\n?\\|\n[^$B!%(B.$B!#(B?$B!)(B\n]\\|[.?][^ 	\n]\\)+$B$J(B\n?[$B$$$/$7(B]")

(se-doc/define-method se-doc/j/youni+deny (beg end cat)
  (let* ((s-beg (se-doc/this-sentence-beg beg 7)))
    (se-doc/simple-display-string (buffer-substring-no-properties s-beg end))))

;;; $BB?=EH]Dj(B
(se-doc/define-category se-doc/j/multi-deny
  "$B$J(B\n?[$B$$$/$7(B]\n?\\([^$B!%(B.$B!#(B?$B!)(B\n$B$J(B]\\|$B$J(B\n?[^$B$$(B]\n?\\|\n[^$B!%(B.$B!#(B?$B!)(B\n]\\|[.?][^ 	\n]\\)+$B$J(B\n?[$B$$$/$7(B]")

(se-doc/define-method se-doc/j/multi-deny se-doc/j/youni+deny)

(se-doc/define-documentation se-doc/j/multi-deny "
$B"O(B L [  L$B$,L5L7=b(B $B"M(B L |- {$B"O(Bq $B":(B S [ $B"L"L(Bq $B"N(B q ]} ]")

;;; $BJ#?t$N!V$N!W(B
(se-doc/define-category se-doc/j/multi-no
  "$B$N(B\n?\\([^,$B!$!%(B.$B!#(B?$B!)(B!$B!*(B\n$B$N(B]\\|\n[^,$B!$!%(B.$B!#(B?$B!)(B\n]\\|[.?!$B!*(B][^ 	\n]\\)+$B$N(B\n?\\([^,$B!$(B.$B!%!#(B?$B!)(B!$B!*(B\n$B$N(B]\\|\n[^,$B!$(B.$B!%!#(B?$B!)(B!$B!*(B\n]\\|[.?!$B!*(B][^ 	\n]\\)+$B$N(B")

(se-doc/define-method se-doc/j/multi-no (beg end cat)
  (let* ((s-beg (se-doc/this-sentence-beg beg 7))
	 (phrase (buffer-substring-no-properties s-beg end)))
    (and (se-doc/valid-multi-no-p phrase)
	 (se-doc/simple-display-string phrase))))

(defun se-doc/valid-multi-no-p (str)
  (setq str (se/remove-regexp-in-string "\\($B!$(B\\|, \\|$B!"(B\\).*" str))
  (setq str (se/remove-regexp-in-string "\\([$B$"$=$3$J$I$b(B]$B$N(B\\|$B$N(B[$B$_$G(B]\\)" str))
  (setq str (se/remove-regexp-in-string "[^$B$N(B]+" str))
  (< 4 (string-width str)))

;;; $BBg?M$N>o<1(B
(se-doc/define-category se-doc/j/machigai
  "$B$D(B\n?$B$D(B\n?$B$E(B\\|"
  "$B$&(B\n?$B$k(B\n?$B3P(B\n?$B$((B"
)

(se-doc/define-method se-doc/j/machigai (beg end cat)
  (se/set-face beg end nil nil)
  (buffer-substring-no-properties beg end))

;;; menu support
(defvar se-jpn/menu-map nil)
(setq se-jpn/menu-map (make-sparse-keymap "Check Japanese"))

(define-menu-order se-jpn/menu-map nil
  [youni+deny]
  '("youni+deny($B$h$&$K!\H]Dj(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/youni+deny)))
  [ukemi]
  '("ukemi($B<u?HI=8=(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/ukemi)))
  [toritate]
  '("toritate($B$H$j$?$F;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/toritate)))
  [suushi]
  '("number($B?t;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/suushi)))
  [multi-no]
  '("multiple no($BJ#?t$N!V$N!W(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/multi-ha)))
  [katakana] 
  '("katanaka($B%+%?%+%J(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/katakana)))
  [1st-term]
  '("1st jargon($B=i=P=Q8l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/1st-term)))
  [multi-ha]
  '("multiple ha($BJ#?t$N!V$O!W(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/multi-ha)))
  [setsuzoku-ga]
  '("ga as conj($B@\B3;l$N!V$,!W(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/setsuzoku-ga)))
  [multi-deny]
  '("multi deny($BB?=EH]Dj(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/multi-deny)))
  [alphabet] 
  '("English word($B1QC18l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/alphabet)))
  [machigai]
  '("mistake($BBg?M$N>o<1(B)" . (lambda () (interactive) (se-doc/check-document se-doc/j/machigai)))
    )
(fset 'se-jpn/menu-map (symbol-value 'se-jpn/menu-map))
(if (boundp 'se-doc/menu-map)
    (define-key se-doc/menu-map [japanese] '("Japanese" . se-jpn/menu-map)))

(provide 'se-jpn)

;; (put 'se-doc/define-method 'lisp-indent-function 2)
;; (put 'se-doc/define-category 'lisp-indent-function 1)
;; (put 'se-doc/define-documentation 'lisp-indent-function 1)

;; Local Variables:
;; auto-fill-function: nil
;; texing-aux-keyword-list:(se-doc/define-category se-doc/define-method)
;; End:
;; se-jpn.el ends here