;;; se-eng.el -*-emacs-lisp-*-  -*-coding: iso-2022-7bit; -*-
;;; English browser
;;; Author: Shuji Narazaki (narazaki@InetQ.or.jp)
;;; Version 0.18
;;; Time-stamp: <2005-06-29 14:42:20 narazaki>
;;; Keywords: text

;;; Commentary:
;;;  Experimental Implementation.
;;; WARNING:
;;; This program is not allowed to copy, modify and redistribute UNLESS THE
;;; VERSION IS GREATER THAN 1.0.

;;; Code:

(eval-and-compile
  (require 'summarye)
  (require 'se-doc))

(defconst se-doc/e/article "A$B4';l(B")
(defconst se-doc/e/jodoushi "J$B=uF0;l(B")
(defconst se-doc/e/not "D$BH]Dj(B")
(defconst se-doc/e/fukushi "F$BI{;l(B")
(defconst se-doc/e/abstract-noun "N$BCj>]L>;l(B")
(defconst se-doc/e/pp-after-noun "P$B8e$m$+$i3]$+$k2a5nJ,;l(B")
(defconst se-doc/e/prep "Z$BA0CV;l(B")
(defconst se-doc/e/ukemi "Be$BF0;l(B+$B2a5nJ,;l(B")
(defconst se-doc/e/England-style "AS$BJF9q<0(B")
(defconst se-doc/e/American-style "ES$B1Q9q<0(B")
(defconst se-doc/e/similars "S$B;w$?C18l(B")
(defconst se-doc/e/uncountable "U$BHs2C;;L>;l(B")

(setq se-doc/show-at-second-line 
      (append (list se-doc/e/article se-doc/e/jodoushi se-doc/e/fukushi
		    se-doc/e/abstract-noun se-doc/e/pp-after-noun)
	      se-doc/show-at-second-line))

(defvar marker)

(defun se-eng/normal-method (beg end cat &optional face postfix prefix)
  (let ((beg* beg)
	(end* end))
    (set-marker marker beg)
    (if face (se/set-face beg end nil t))
    (when postfix
      (goto-char end)
      (forward-word postfix)
      (setq end* (point)))
    (when prefix
      (goto-char beg)
      (backward-word prefix)
      (setq beg* (point)))
    (goto-char end)
    (se-doc/canonical-sentence (buffer-substring-no-properties beg* end*))))

;; * $B4';l(B
(se-doc/define-category se-doc/e/article t ; t means case-fold
  "\\b\\(an?\\|the\\)\\b")

(se-doc/define-method se-doc/e/article (beg end cat)
  (se-eng/normal-method beg end cat t 1))

;; * $B=uF0;l(B
(se-doc/define-category se-doc/e/jodoushi t
  "\\b\\(can\\|could\\|may\\|might\\|shall\\|should\\|will\\|would\\|ought\\Wto\\)\\b")

(se-doc/define-method se-doc/e/jodoushi (beg end cat)
  (se-eng/normal-method beg end cat t 1 1))

;; * $BH]Dj(B
(se-doc/define-category se-doc/e/not t
  "\\b\\(not\\)\\b")

(se-doc/define-method se-doc/e/not (beg end cat)
  (se-eng/normal-method beg end cat t 1 1))

;; * $BI{;l(B
(se-doc/define-category se-doc/e/fukushi
 t
 "\\b\\("
 "binterestingly\\|"			; 97%
 "unfortunately\\|"			; 96%
 "fortunately\\|"			; 92%
 "consequently\\|"			; 80%
 "similary\\|"				; 64%
 "in\\wfact\\|"				; 63%
 "likewise\\|"				; 57%
 "indeed\\|"				; 46%
 "evidently\\|"				; 40%
 "obviously\\|"				; 38%
 "apparently\\|"			; 24%
 "presumably\\|"			; 18%
 "certainly\\|"				; 14%
 "clearly\\|"				; 13%
 "accordingly\\|"			; 100%
 "furthermore\\|"			; 95%
 "alternatively\\|"			; 94%
 "additionally\\|"			; 93%
 "conversely\\|"			; 91%
 "nevertherless\\|"			; 88%
 "moreover\\|"				; 88%
 "finally\\|"				; 86%
 "nonetheless\\|"			; 75%
 "consequently\\|"			; 73%
 "however\\|"				; 69%
 "hence"				; 61%
 "\\)\\b")

(se-doc/define-method se-doc/e/fukushi se-doc/e/article)

(se-doc/define-documentation se-doc/e/fukushi "
$B0J2<$N%G!<%?$O(B120$B%Q!<%;%s%H2J3X1Q8l$K$h$k!%%Q!<%;%s%F!<%8$OJ8F,$K$*$+$l$k(B
$B3NN(!%(B
 binterestingly			 97%
 unfortunately			 96%
 fortunately			 92%
 consequently			 80%
 similary			 64%
 in fact			 63%
 likewise			 57%
 indeed				 46%
 evidently			 40%
 obviously			 38%
 apparently			 24%
 presumably			 18%
 certainly			 14%
 clearly			 13%
 accordingly			100%
 furthermore			 95%
 alternatively			 94%
 additionally			 93%
 conversely			 91%
 nevertherless			 88%
 moreover			 88%
 finally			 86%
 nonetheless			 75%
 consequently			 73%
 however			 69%
 hence				 61%")

;; * $BCj>]L>;l(B + of
(se-doc/define-category se-doc/e/abstract-noun
  t
  "\\b\\("				; $BDj4';l(B $BL54';l(B $BITDj4';l(B
  "presence\\|"				; 100%
  "question\\|"				; 100%
  "aim\\|"				; 100%
  "purpose\\|"				; 100%
  "mechanism\\|"			;  98%    1%     1%
  "possibility\\|"			;  97%    3%
  "ability\\|"				;  95%    5%
  "lack\\|"				;  95%    1%     4%
  "activity\\|"				;  92%    6%     2%
  "behavior\\|"				;  89%    8%     3%
  "instability\\|"			;  84%   12%     4%
  "action\\|"				;  83%   13%     4%
  "ease\\|"				;  82%   18%
  "advantage\\|"			;  80%   20%
  "use\\|"				;  74%   26%
  "result\\|"				;  74%   26%
  "reaction\\|"				;  68%   27%     5%
  "application\\|"			;  56%   39%     5%
  "introduction\\|"			;  55%   45%
  "decomposition\\|"			;  50%   50%
  "addition\\|"				;  42%   56%     2%
  "removal\\|"				;  18%   82%
  "analysis\\|"				;  13%   74%    13%
  "examination\\|"			;   8%   66%    26%
  "treatment\\|"			;   6%   92%     2%
  "workup"				;   0%  100%
  "\\)\\wof\\b")

(se-doc/define-method se-doc/e/abstract-noun se-doc/e/article)
(se-doc/define-documentation se-doc/e/abstract-noun "
$B0J2<$N%G!<%?$O(B120$B%Q!<%;%s%H2J3X1Q8l$K$h$k!%%Q!<%;%s%F!<%8$OJ8F,$K$*$+$l$k(B
$B3NN(!%(B
			; $BDj4';l(B $BL54';l(B $BITDj4';l(B
  presence		; 100%
  question		; 100%
  aim			; 100%
  purpose		; 100%
  mechanism		;  98%    1%     1%
  possibility		;  97%    3%
  ability		;  95%    5%
  lack			;  95%    1%     4%
  activity		;  92%    6%     2%
  behavior		;  89%    8%     3%
  instability		;  84%   12%     4%
  action		;  83%   13%     4%
  ease			;  82%   18%
  advantage		;  80%   20%
  use			;  74%   26%
  result		;  74%   26%
  reaction		;  68%   27%     5%
  application		;  56%   39%     5%
  introduction		;  55%   45%
  decomposition		;  50%   50%
  addition		;  42%   56%     2%
  removal		;  18%   82%
  analysis		;  13%   74%    13%
  examination		;   8%   66%    26%
  treatment		;   6%   92%     2%
  workup		;   0%  100%
")

;; * $B8e$m$+$i3]$+$k2a5nJ,;l(B
(se-doc/define-category se-doc/e/pp-after-noun
 t
 "\\b\\("
 "employed\\|"
 "used\\|"
 "utilized\\|"
 "described\\|"
 "shown\\|"
 "studied\\|"
 "investigated\\|"
 "tested\\|"
 "examined\\|"
 "involved\\|"
 "formed\\|"
 "produced\\|"
 "synthesized\\|"
 "prepared\\|"
 "obtained\\|"
 "isolated"
 "\\)\\b")

(se-doc/define-method se-doc/e/pp-after-noun (beg end cat)
  (se-eng/normal-method beg end cat t 1 1))

(se-doc/define-documentation se-doc/e/pp-after-noun "
$B0J>e$N%G!<%?$O(B120$B%Q!<%;%s%H2J3X1Q8l$K$h$k(B")

;; * $BA0CV;l(B
(se-doc/define-category se-doc/e/prep t
  "\\b\\("
  "information\\|"
  "study\\|"
  "investigation\\|"
  "data\\|"
  "reports?\\|"
  "support\\|"
  "evidence\\|"
  "methods?\\|"
  "reason\\|"
  "opportunity\\|"
  "opportunities\\|"
  "strategy\\|"
  "strategies\\|"
  "methodology\\|"
  "methodologies\\|"
  "procedures?\\|"
  "changes?\\|"
  "entry\\|"
  "access\\|"
  "routes?\\|"
  "alternatives?\\)"
  "\\W\\(in\\|into\\|for\\|of\\|on\\|to\\|under\\)\\b")

(se-doc/define-method se-doc/e/prep se-doc/e/article)

(se-doc/define-documentation se-doc/e/prep "
$B0J2<$N%G!<%?$O(B120$B%Q!<%;%s%H2J3X1Q8l$K$h$k!%(B
access:		to(51)
alternative:	to(21) for preparation(3)
approach:	to(33$B2=9gJ*(B) for(3$B9g@.(B)
change:		in(143) of (27) for(9$B!V!A$KBP$9$kJQ2=!W(B)
data:		for(150) on(32)  about(1) 
entry:		to(22) into(11)
evidence:	for(174) of(47) in(7$BNc(B;$B>l=j!$=O8l(B)
information:	on(34$BNc(B) about(24$BNc(B)
investigation:	of(62$BNc(B) on(4) into(4) 
method:		for(49) of(11) in(3)
methodology:	for(17)
need:		for(33) of93) to + v.(26)
opportunity:	for(15) to + v.(14)
procedure:	for(43) of + $B?ML>(B(31)
reason:		for(54) to + v.(16)
report:		of(13) on(10) concerning(2) regarding(1) about(1)
route:		to(37$B2=9gJ*(B) for(5$B9g@.(B)
strategy:	for(15) of(4) in(2)
study:		of(150$BNc(B) on(23$BNc(B)
support:	for(41) of(4) in(0)
")

;; * $B<u?H(B
(se-doc/define-category se-doc/e/ukemi t
  "\\b\\(am\\|are\\|is\\|was\\|were\\|be\\|been\\)\\W"
  "\\\([a-z]+ed\\|"
  "broken\\|brought\\|built\\|"
  "chosen\\|cut\\|"
  "dealt\\|done\\|found\\|fled\\|forecast\\|forgotten\\|"
  "known\\|"
  "run\\|"
  "shown\\|"
  "taken\\|taught\\|told\\|thought\\"
  "written\\)")
(se-doc/define-method se-doc/e/ukemi se-doc/e/article)
(se-doc/define-documentation se-doc/e/ukemi "
$BBh(B1$BHG(B
")

;; * $B1Q9q<0(B
(se-doc/define-category se-doc/e/England-style t
  "\\b\\("
  "behaviour\\|"
  "catalogue\\|"
  "centre\\|"
  "grey\\|"
  "marvellous\\|"
  "skilful\\|"
  "speciality\\|"
  "travelling"
  "\\)")

(se-doc/define-method se-doc/e/England-style se-doc/e/article)
(se-doc/define-documentation se-doc/e/England-style
  "
$B1Q9q<0DV(B	$BJF9q<0DV(B
behaviour	behavior
catalogue	catalog
centre		center
grey		gray
marvellous	marvelous
skilful		skillful
speciality	specialty
travelling	taveling
")

;; * $B;w$?8@MU(B
(se-doc/define-category se-doc/e/similars t
  "\\b\\("
  "accep\\(ts?\\|ted\\|ting\\)\\|except\\|"
  "adap\\(ts?\\|ted\\|ting\\)\\|adop\\(ts?\\|\\|ted\\|ting\\)\\|"
  "advices?\\|advis\\(es?\\|ed\\|ing\\)\\|"
  "affec\\(ts?\\|ted\\|ting\\)\\|effects?\\|"
  "complement\\|compliments?\\|"
  "dependants?\\|dependent\\|"
  "devices?\\|devis\\(es?\\|ed\\|ing\\)\\|"
  "discreet\\|discrete\\|"
  "preced\\(d\\|ed\\|ing\\)\\|procee\\(d\\|ded\\|ding\\)\\|"
  "principal\\|principle\\|"
  "stationary\\|stationerys?"
  "\\)\\b")
(se-doc/define-method se-doc/e/similars se-doc/e/article)
(se-doc/define-documentation se-doc/e/similars
  "
  accept($BF0;l(B)			except($BA0CV;l(B)
  adapt($BE,1~$5$;$k(B)		adopt($B:NMQ$9$k(B)
  advice($BL>;l(B)			advise($BF0;l(B)
  affect($BF0;l(B)			effect($BL>;l(B)
  complement($BA4BN$N;D$j(B)	compliment($B$*@$<-(B)
  dependant($BL>;l(B)		dependent($B7AMF;l(B)
  device($BL>;l(B)			devise($BF0;l(B)
  discreet($B?5=E$J(B)		discrete($BN%;6E*$J(B)
  precede($B@h$s$:$k(B)		proceed($BA0?J$9$k(B)
  principal($B<g$J(B)		principle($B86B'(B)
  stationary($B@E;_$7$?(B)		stationery($BJ8K<6q(B)
")

;; * $BHs2C;;L>;l(B
(se-doc/define-category se-doc/e/uncountable t
  "\\b\\("
  "information\\|"
  "equipment\\|"
  "knowledge\\|"
  "evidence\\|"
  "advice\\|"
  "encouragement\\|"
  "asistance\\|"
  "help\\|"
  "health\\|"
  "dependence\\|"
  "behavior\\|"
  "abilit\\(y\\|ies\\)"
  "\\)")
(se-doc/define-method se-doc/e/uncountable se-doc/e/article)

;; * $B%$%s%?!<%U%'%$%9(B

(defvar se-eng/menu-map (make-sparse-keymap "Check English"))
(define-menu-order se-eng/menu-map nil
  [article]    
  '("pre($B4';l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/article)))
  [jodoushi]     
  '("assc verb($B=uF0;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/jodoushi)))
  [not]     
  '("deny($BH]Dj(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/not)))
  [fukushi]
  '("adv($BI{;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/fukushi)))
  [abst-noun]
  '("abst. noun($BCj>]L>;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/abstract-noun)))
  [pp-after-noun]
  '("noun+pp($BL>;l!\2a5nJ,;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/pp-after-noun)))
  [prep]
  '("prep($BA0CV;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/prep)))
  [ukemi]
  '("be+pp(be$BF0;l(B+$B2a5nJ,;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/ukemi)))
  [similars]
  '("similar word($B;w$?C18l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/similars)))
  [England-style]
  '("English spell($B1Q9q<0DV(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/England-style)))
  [uncountable]
  '("uncountable($BHs2C;;L>;l(B)" . (lambda () (interactive) (se-doc/check-document se-doc/e/uncountable)))
  )
(fset 'se-eng/menu-map (symbol-value 'se-eng/menu-map))
(if (boundp 'se-doc/menu-map)
    (define-key se-doc/menu-map [english] '("English" . se-eng/menu-map)))

(provide 'se-eng)

;; (put 'se-doc/define-method 'lisp-indent-function 2)
;; (put 'se-doc/define-category 'lisp-indent-function 2)
;; (put 'se-doc/define-documentation 'lisp-indent-function 1)
;; Local Variables:
;; se/item-delimiter-regexp: "^;; \\* \\(.*\\)$"
;; End:
;; se-eng.el ends here